from dash import Dash, dcc, html, Input, Output, State
from dash.dependencies import Input, Output, ALL, State, MATCH, ALLSMALLER
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas as pd
import numpy as np


app = Dash(__name__, external_stylesheets=[dbc.themes.SKETCHY])
df = pd.read_excel("it-16-30-onet-p6-stats-by-content-by-province.xlsx")
df.columns = [
            'YEAR_COURSE', 
            'จังหวัด', 
            'ชื่อวิชา', 
            'sub_category', 
            'en_sub_category', 
            'คะแนนเต็ม','คะแนนเฉลี่ย','2','3','4','5','6']

df["คะแนนเต็ม"] = df["คะแนนเต็ม"].replace("-", 0)


@app.callback(
            Output("pie-graph","figure"), 
            Input('year','value'),
            Input("จังหวัด", "value"),
            Input("choice", "value"),
            )

def update_graph(selected_year, province, chart_choice):
    filtted_data = df[df['YEAR_COURSE'] == int(selected_year)] [df["จังหวัด"].str.contains(province)]

    if chart_choice == 'scatter':
        fig = px.scatter(
            filtted_data,
            x = "คะแนนเฉลี่ย", 
            y = "คะแนนเต็ม", 
            color = "ชื่อวิชา",
            title = f'แผนภูมิจุดแบบกระจัดกระจายแสดงคะแนนเฉลี่ย O-NET ปี {selected_year} ของพื้นที่จังหวัด {province}'
            ).update_layout(
                {"plot_bgcolor": '#e5da6c', "paper_bgcolor": '#40ba9e'}  
                            )
            # color_discrete_map={'Thur':'lightcyan',
            #                         'Fri':'cyan',
            #                         'Sat':'royalblue',
            #                         'Sun':'darkblue'})
            # fig.update_traces(textposition='inside', textinfo='percent+label')

        return fig

    elif chart_choice == 'pie':
        fig = px.pie(filtted_data,
                    values = 'คะแนนเฉลี่ย', 
                    names = 'ชื่อวิชา',
                    title = f'แผนภูมิวงกลมแสดงคะแนนเฉลี่ย O-NET ปี {selected_year} ของพื้นที่จังหวัด {province}',
                    color = 'ชื่อวิชา',
                    color_discrete_map={'วิทยาศาสตร์':'#0d3b66',
                                        'คณิตศาสตร์':'#cddafd',
                                        'ภาษาไทย':'#f4d35e',
                                        'ภาษาอังกฤษ':'#f95738',} ).update_layout(
                                            {"plot_bgcolor": '#e5da6c', "paper_bgcolor": '#40ba9e'}
                                        )
        fig.update_traces(textposition = 'inside',
                        textinfo = 'percent+label')
                     
        return fig

app.layout = html.Div(
    style={'width': '100%', 'display': 'inline-block', 'padding': 68,'background-color': '#7978b1'},
    children=[
        html.Div(
            children=[
                html.H1(
                    children = "คะแนนเฉลี่ย O-NET",
                    style = {
                        "textAlign" : "center",
                        "background-color": "#f5f5dc",
                        "border": "5px outset #c39797",
                        "color" : "#696969",
                        "margin-top":"40px"
                    },
                )
            ],
            className = "row",
        ),
        
        html.Div(
            children = [
                html.H2(
                    children = "ระหว่างปี 2561-2564 ทั่วประเทศ",
                    style = {
                        "textAlign": "center",
                        "background-color": "#faebd7",
                        "border": "5px outset #cbbeb5",
                        "color": "#808080",
                        "margin-top":"5px"
                    },
                ),
            ],
            className = "row",
        ),

        html.Div(
            [
                html.Div(
                    [
                        dcc.Graph(id = "pie-graph")
                    ],
                    className = "col-8",
                ),
                html.Div(
                    [
                        dbc.Label("Select Year and Province", size= 'lg',style= dict(marginLeft=10, marginTop = 50), color='#ee749b'),
                        dbc.Select(
                            id = "year",
                            value = df["YEAR_COURSE"].min(),
                            options = [{'label': s, 'value': s} for s in np.sort(df['YEAR_COURSE'].unique())],    
                        ),
                       
                        dbc.Select(
                            id = "จังหวัด",
                            value = df["จังหวัด"][0],
                            options = [
                                dict(label = et.strip(), value = et.strip())
                                for et in df["จังหวัด"].unique()
                            ],
                        ),

                        dbc.Label("Select Chart", size = 'lg', style = dict(marginLeft = 10, marginTop = 20), color = '#ee749b'),
                        dcc.RadioItems(
                            options = [
                                {
                                    'label': html.Div(['Scatter Chart'], style = {'color': '#e5da6c', 'font-size': 18, 
                                                                        "margin-top":"-12px", 'padding-left': 20,}), 
                                    'value': 'scatter'
                                },
                                {
                                    'label': html.Div(['Pie Chart'], style = {'color': '#40ba9e', 'font-size': 18, 
                                                                        "margin-top":"-12px", 'padding-left': 20,}),  
                                    'value': 'pie'
                                }
                            ],
                            value = 'scatter',
                            id = ('choice'), inline = False , labelStyle={'display': 'block','margin-top':'-10px'},
                        ),
                    ],
                    className = "col-4",
                ),
            ],
            className = "row",
        ),
    ],
    className = "container-fluid",
)

        
if __name__ == '__main__':
    app.run_server(debug = True)